package com.aparshikhin.developmentlife.mem.datasource

import com.aparshikhin.developmentlife.mem.model.Mem
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author andrey.parshikhin
 */
interface IMemService {

    /**
     * Метод для получения случайного мема.
     *
     * @param json - параметр отвечает за json формат в response.
     *
     * @return модель мема.
     *
     * @author andrey.parshikhin
     */
    @GET("random")
    suspend fun getRandomMem(@Query("json") json: Boolean = true): Mem

}