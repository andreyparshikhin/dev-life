package com.aparshikhin.developmentlife.mem

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.aparshikhin.developmentlife.mem.model.Mem
import com.aparshikhin.developmentlife.mem.repository.IMemRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author andrey.parshikhin
 */
@HiltViewModel
internal class MemViewModel @Inject constructor(
    private val memRepository: IMemRepository
) : ViewModel() {

    val memLiveData: MutableLiveData<Mem> by lazy {
        MutableLiveData<Mem>()
    }

    /**
     * Метод для получения случайного мема.
     *
     * @author andrey.parshikhin
     */
    // TODO проверка кейса, когда выключен internet
    fun requestMem() = viewModelScope.launch(Dispatchers.IO) {
        val mem = memRepository.getRandomMem()
        Log.d("OkHttp", mem.toString())
        memLiveData.postValue(mem)
    }
}