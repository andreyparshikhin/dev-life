package com.aparshikhin.developmentlife.mem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.aparshikhin.developmentlife.R
import com.aparshikhin.developmentlife.databinding.FragmentMemeBinding
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint

/**
 * Фрагмент для отображения мемов.
 *
 * @author andrey.parshikhin
 */
@AndroidEntryPoint
internal class MemFragment : Fragment() {

    private var binding: FragmentMemeBinding? = null
    private val viewModel: MemViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMemeBinding.inflate(inflater, container, false)
        binding!!.randomButton.setOnClickListener { viewModel.requestMem() }

        viewModel.memLiveData.observe(viewLifecycleOwner) { mem ->
            binding!!.memText.text = mem.description

            val circularProgressDrawable = CircularProgressDrawable(binding!!.root.context)
            circularProgressDrawable.centerRadius = 50f
            circularProgressDrawable.strokeWidth = 5f
            circularProgressDrawable.start()

            Glide.with(this)
                .asGif()
                .load(mem.gifURL)
                .placeholder(circularProgressDrawable)
                .error(R.drawable.ic_baseline_image_not_supported_24)
                .centerCrop()
                .into(binding!!.memImage)
        }

        viewModel.requestMem()

        return binding!!.root
    }
}