package com.aparshikhin.developmentlife.mem.repository

import com.aparshikhin.developmentlife.mem.datasource.IDataSource
import com.aparshikhin.developmentlife.mem.model.Mem
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author andrey.parshikhin
 */
@Singleton
class MemRepository @Inject constructor(private val dataSource: IDataSource) : IMemRepository {

    override suspend fun getRandomMem(): Mem = dataSource.getRandomMem()

}