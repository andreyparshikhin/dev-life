package com.aparshikhin.developmentlife.mem.datasource

import com.aparshikhin.developmentlife.mem.model.Mem

/**
 * @author andrey.parshikhin
 */
interface IDataSource {

    /**
     * Метод для получения случайного мема.
     *
     * @return модель мема.
     *
     * @author andrey.parshikhin
     */
    suspend fun getRandomMem(): Mem
}