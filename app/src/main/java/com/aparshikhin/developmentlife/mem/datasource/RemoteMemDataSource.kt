package com.aparshikhin.developmentlife.mem.datasource

import com.aparshikhin.developmentlife.mem.model.Mem
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author andrey.parshikhin
 */
@Singleton
class RemoteMemDataSource @Inject constructor(
    private val memService: IMemService
) : IDataSource {

    companion object {
        const val BASE_URL = "https://developerslife.ru/"
    }

    override suspend fun getRandomMem(): Mem = memService.getRandomMem()

}