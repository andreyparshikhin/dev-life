package com.aparshikhin.developmentlife.mem.model

import com.squareup.moshi.Json

/**
 * Модель мема.
 *
 * @author andrey.parshikhin
 */
data class Mem(
    @field:Json(name = "id")
    val id: String,
    @field:Json(name = "description")
    val description: String,
    @field:Json(name = "gifURL")
    val gifURL: String,
)
