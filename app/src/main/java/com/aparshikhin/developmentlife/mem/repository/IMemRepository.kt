package com.aparshikhin.developmentlife.mem.repository

import com.aparshikhin.developmentlife.mem.model.Mem

/**
 * @author andrey.parshikhin
 */
interface IMemRepository {

    /**
     * Метод для получения случайного мема.
     *
     * @return модель мема.
     *
     * @author andrey.parshikhin
     */
    suspend fun getRandomMem(): Mem
}