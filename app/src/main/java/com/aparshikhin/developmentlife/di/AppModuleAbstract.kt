package com.aparshikhin.developmentlife.di

import com.aparshikhin.developmentlife.mem.datasource.IDataSource
import com.aparshikhin.developmentlife.mem.datasource.RemoteMemDataSource
import com.aparshikhin.developmentlife.mem.repository.IMemRepository
import com.aparshikhin.developmentlife.mem.repository.MemRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * @author andrey.parshikhin
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class AppModuleAbstract {

    @Binds
    abstract fun bindIMemRepository(memRepository: MemRepository): IMemRepository

    @Binds
    abstract fun bindIDataSource(remoteMemDataSource: RemoteMemDataSource): IDataSource
}