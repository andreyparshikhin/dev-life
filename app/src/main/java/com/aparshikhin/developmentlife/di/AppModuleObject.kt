package com.aparshikhin.developmentlife.di

import com.aparshikhin.developmentlife.mem.datasource.IMemService
import com.aparshikhin.developmentlife.mem.datasource.RemoteMemDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

/**
 * @author andrey.parshikhin
 */
@Module
@InstallIn(SingletonComponent::class)
object AppModuleObject {

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        val logging = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC)
        val client: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()

        return Retrofit.Builder()
            .baseUrl(RemoteMemDataSource.BASE_URL)
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    }

    @Provides
    @Singleton
    fun provideMemService(retrofit: Retrofit): IMemService =
        retrofit.create(IMemService::class.java)

}