package com.aparshikhin.developmentlife

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * @author andrey.parshikhin
 */
@HiltAndroidApp
class DevelopmentLifeApplication : Application()